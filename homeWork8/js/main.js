const inp = document.querySelector('input').onclick = function () {
    const inp2 = document.createElement('input');
    inp2.setAttribute("placeholder", "Диаметр круга");
    inp2.classList.add("num");
    document.body.append(inp2);

    const createCircle = document.createElement('button');
    createCircle.textContent = "Нарисовать";
    document.body.append(createCircle);


    createCircle.onclick = function () {
        let data = +document.querySelector('.num').value;

        if(data < 50) {
            alert("Введите диамет >= 50");
        }else if(data > 100){
            alert("Введите диамет < 100");
        }else {
            for (let i = 0; i < 100; i++) {
            let div = document.createElement("div");
            div.className = "circle";
            div.style.display = "inline-block";
            div.style.width = `${data}px`;
            div.style.height = `${data}px`;
            div.style.background = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
            div.style.borderRadius = `${data}px`;
            div.style.cursor = "pointer";
            document.body.append(div);
            }
        }
            
        let removeCircles = document.querySelectorAll(".circle");
            for (let i = 0; i < removeCircles.length; i++) {
            removeCircles[i].onclick = function () {
                removeCircles[i].remove();
                }
            }
    }
}