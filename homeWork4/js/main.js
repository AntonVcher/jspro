//1
const myArray = [1,2,3,4,5,6,7];

function map(fn, array) {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        myArray[i] = fn(array[i]);
    }
    return myArray;
}

function fn(x) {
    return x + 2;
  }
  
  document.write(map(fn, myArray) + '<br>');
  document.write('<hr>');

  //2
  function checkAge(age){
    return age > 18? document.write(true) : confirm('Батьки дозволили?');
  }
  checkAge(17);
  checkAge(21);

