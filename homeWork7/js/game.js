let words = [
    "виндовс",
    "андроид",
    "система",
    "атмосфера",
    "пространство",
    "месяц",
    "частица",
];

let word = words[Math.floor(Math.random() * words.length)];

let answerArray = [];
for(let i in word) {
    answerArray[i] = "_";
}

let remainingLetters = words.length;

while(remainingLetters > 0){
    alert(answerArray.join(" "));
    let guess = prompt("Угадай букву или нажми 'Отмена'");
    if (guess === null) {
        break;
    }else if (guess.length !== 1){
        alert("Введите только одну букву!")
    }else {
        for(let j in word) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
}
alert(answerArray.join(" "));
alert("Поздравляем! Загаданое слово" + word);
