// field stopwatch 
const minuteElement = document.querySelector('.minute');
const secondElement = document.querySelector('.second');
const milisecondElement = document.querySelector('.milisecond');

// buttons stopwatch
const startButton = document.querySelector('.start');
const stopButton = document.querySelector('.stop');
const resetButton = document.querySelector('.reset');


let minute = 00,
    second = 00,
    milisecond = 00,
    interval,
    div = document.querySelector(".black")


startButton.addEventListener('click', () => {
    clearInterval(interval)
    interval = setInterval(startStopwatch, 10)  
    div.classList.remove('black');
    div.classList.remove('silver');
    div.classList.remove('red');
    div.classList.add("green");
})

stopButton.addEventListener('click', () => {
    clearInterval(interval)
    div.classList.remove('green');
    div.classList.remove('silver');
    div.classList.add('red');
})

resetButton.addEventListener('click', () => {
    clearInterval(interval)
    milisecond = 00
    second = 00
    minute = 00
    minuteElement.innerText = "00"
    secondElement.innerText = "00"
    milisecondElement.innerText = "00"
    div.classList.remove('green');
    div.classList.remove('red');
    div.classList.add('silver');
})

// function stopwatch
const startStopwatch = () => {
    milisecond++
    if(milisecond < 9) {
        milisecondElement.innerText = "0" + milisecond
    }
    if(milisecond > 9) {
        milisecondElement.innerText = milisecond
    }
    if(milisecond > 99) {
        second++
        secondElement.innerText = "0" + second
        milisecond = 0
        milisecondElement.innerText = "0" + milisecond
    }

    if(second < 9) {
        secondElement.innerText = "0" + second
    }
    if(second > 9) {
        secondElement.innerText = second
    }
    if(second > 59) {
        minute++
        minuteElement.innerText = '0' + minute
        second = 0 
        secondElement.innerText = "0" + second
    }
}






