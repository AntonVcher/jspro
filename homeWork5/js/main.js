const docum = {
    title: "Заголовок",
    body: "Тело",
    footer: "Футер",
    data: "Дата",
    app: {
        title: {
            title1: "Заголовок приложения"
        },
        body: {
            body1: "Тело приложения"
        },
        footer: {
            footer1: "футер приложения"
        },
        data: {
            data1: "Дата приложения"
        },
    },
    show: function () {
        document.getElementById('doc').innerHTML = `Это ${this.title} документа, <br> это ${this.body} документа, <br> это ${this.footer} документа, <br> это ${this.data} документа. <br> <hr>`
        document.getElementById('app').innerHTML = `А это ${this.app.title.title1} приложения, <br> это ${this.app.body.body1} приложения, <br> это ${this.app.footer.footer1} приложения, <br> это ${this.app.data.data1} приложения.`
    }
}

docum.show()