// 1
function Human (name, age, city) {
    this.age = age;
    this.name = name;
    this.city = city;
}

let humens = [
    new Human("Alex", 34, "Toronto"),
    new Human("Rob", 29, "London"),
    new Human("Michel", 68, "Tokio"),
    new Human("Tom", 18, "Berlin")
];

function sortByAge (arr) {
    arr.sort((a, b) => a.age > b.age ? 1 : -1);
}

sortByAge(humens)


for(let i = 0; i < humens.length; i++) {
    document.write(`Humen ${humens[i].name} is ${humens[i].age} years old. <br> <hr>`); 
}

document.write(`<br> <br> <br>`)





//2 
function Humen(name, food) {
  this.name = name;
  this.food = food;

  this.favoriteFood = function () {
    document.write(
      `Hello, my name is ${this.name}, and my favorite food is ${this.food} <br/>`,
    );
  };
}

Humen.prototype.letsEat = function () {
  return `Hey ${this.name}. Lets it some ${this.food} <br/> <hr>`;
};

const humen1 = new Humen('Don', 'Pizza');
const humen2 = new Humen('Rick', 'Cake');

document.write(humen1.letsEat());

humen1.favoriteFood();
humen2.favoriteFood();